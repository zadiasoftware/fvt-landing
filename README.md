# FVT-Landing #

The project is a bootstrap-based responsive landing page for FashionVIPTeam (http://fashionvipteam.es/), while they launch the final online shop.

It is minimalistic, as the purpose is only showcasing some of the services provided by FVT and let people know something about the project until it's officially launched.


![2014-09-11 18.25.51.png](https://bitbucket.org/repo/BGazxB/images/4286574108-2014-09-11%2018.25.51.png)